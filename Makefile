BASE_DIR=$(shell pwd)

-include .make/base.mk
-include .make/release.mk
-include .make/oci.mk
-include .make/helm.mk
-include .make/k8s.mk

-include PrivateRules.mak

NAME=jupyterhub
